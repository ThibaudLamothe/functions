# -*- coding: utf-8 -*-
from json import loads as __load

# Giving the path where to find all local information
__local_json = '/Users/thibaudlamothe/Documents/Python_scripts/00_Fonctions/json_files/local.json'

# Loading that json file
with open(__local_json, "r") as __fichier:
    __file =  __fichier.read()

# Making the json conversion
__json_file = __load(__file)

#Affecting variables
path_list = __json_file['path']
datasets_path = __json_file['path']['datasets']